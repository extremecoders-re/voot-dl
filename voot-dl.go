package main

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"encoding/binary"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"strings"

	"github.com/grafov/m3u8"
)

var bitrate uint
var startSeq uint64
var outfile *os.File
var streamno uint
var proxyUrl string

/*
Checks whether a given URL is absolute or relative
*/
func isAbsoluteUrl(url string) bool {
	if strings.HasPrefix(url, "http") {
		return true
	}
	return false
}

/*
 Downloads a file and returns  the content as a byte array
*/
func downloadFile(url string) []byte {
	resp, err := http.Get(url)

	if err != nil {
		logOnly(err.Error())
		return nil
	}

	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	return body
}

/*
 Decrypt the buffer in place
*/
func decrypt(buf []byte, key []byte, iv []byte) {
	block, _ := aes.NewCipher(key)
	cfb := cipher.NewCBCDecrypter(block, iv)

	//	plaintext := make([]byte, len(ciphertext))
	//	cfb.CryptBlocks(plaintext, ciphertext)
	cfb.CryptBlocks(buf, buf)
}

/*
 Generates the Initialization vector from the sequence number
*/
func getIV(seq uint64) []byte {
	iv := make([]byte, 16)

	//actually iv is 128 bit, but we are considering the lower 64 bits only
	binary.BigEndian.PutUint64(iv[8:], seq)
	return iv
}

/*
 Reads the encryption key file from url
*/
func getEncryptionKey(url string) []byte {
	contents := downloadFile(url)
	if contents == nil {
		logAndQuit(fmt.Sprint("The specified key url does not exists", url))

	} else if len(contents) != 16 {
		logAndQuit("Invalid key size")
	}
	return contents
}

/*
 Parse the media playlist at the url
*/
func parseMediaPlaylist(url string) {
	contents := downloadFile(url)
	if contents == nil {
		logAndQuit(fmt.Sprint("Invalid url", url))
	}

	playlist, listType, _ := m3u8.Decode(*bytes.NewBuffer(contents), false)

	if listType != m3u8.MEDIA {
		logAndQuit("WTF? This has to be a MEDIA playlist")
	}
	logOnly("Parsing media playlist")

	mediaPl := playlist.(*m3u8.MediaPlaylist)

	// If start seq is not specified
	if startSeq == 0 {
		startSeq = mediaPl.SeqNo
	}

	if mediaPl.Key.Method != "AES-128" {
		logAndQuit(fmt.Sprint("Unsupported encryption method", mediaPl.Key.Method))
	}

	var keyURI string
	if isAbsoluteUrl(mediaPl.Key.URI) {
		keyURI = mediaPl.Key.URI
	} else {
		pos := strings.LastIndex(url, "/")
		if pos != -1 {
			keyURI = url[:pos+1] + mediaPl.Key.URI
		} else {
			logAndQuit("Invalid key url")
		}
	}

	key := getEncryptionKey(keyURI)
	var seq uint64 = mediaPl.SeqNo
	for _, segment := range mediaPl.Segments {
		if segment == nil {
			break
		}

		if seq >= startSeq {
			var mediapartURL string

			if isAbsoluteUrl(segment.URI) {
				mediapartURL = segment.URI
			} else {
				pos := strings.LastIndex(url, "/")
				if pos != -1 {
					mediapartURL = url[:pos+1] + segment.URI

				}
			}

			logOnly(fmt.Sprintf("Downloading %s (seq %d)...", segment.URI, seq))
			buf := downloadFile(mediapartURL)
			if buf == nil {
				logOnly("Failed\n")
				continue
			}

			// Initialization vector
			iv := getIV(seq)
			decrypt(buf, key, iv)
			outfile.Write(buf)
			outfile.Sync()
			logOnly("Complete\n")

		} else {
			logOnly(fmt.Sprintf("Invalid url of part %s. Skipping\n", segment.URI))
		}
		seq++
	}
}

/*
 Parse the master playlist at the url
*/
func parseMasterPlaylist(url string) {
	var idx uint = 1
	playlist, listType, _ := m3u8.Decode(*bytes.NewBuffer(downloadFile(url)), false)

	if listType != m3u8.MASTER {
		logAndQuit("WTF? This has to be a MASTER playlist")
	}
	logOnly("Parsing master playlist")

	masterPl := playlist.(*m3u8.MasterPlaylist)

	for _, variant := range masterPl.Variants {
		if variant == nil {
			break
		}
		if variant.VariantParams.Bandwidth == uint32(bitrate) || streamno == idx {
			parseMediaPlaylist(variant.URI)
			return
		}
		idx += 1
	}
	logOnly(fmt.Sprintf("No stream of bitrate %d exists. Listing available bitrates\n", bitrate))
	listBitrates(url)
}

/*
 Lists the available bitrates in a hls stream
*/
func listBitrates(url string) {
	contents := downloadFile(url)
	if contents == nil {
		logAndQuit(fmt.Sprint("Invalid url", url))
	}

	playlist, listType, _ := m3u8.Decode(*bytes.NewBuffer(contents), false)
	if listType != m3u8.MASTER {
		logAndQuit("WTF? This has to be a MASTER playlist")
	}

	masterPl := playlist.(*m3u8.MasterPlaylist)

	for _, variant := range masterPl.Variants {
		fmt.Printf("Bitrate=%d  Resolution=%s  Codecs=%s\n", variant.VariantParams.Bandwidth, variant.VariantParams.Resolution, variant.VariantParams.Codecs)
	}
}

/*
 Fetch the playlist from the url
*/
func getPlaylist(url string) (m3u8.ListType, uint) {
	var numstreams uint = 0

	contents := downloadFile(url)
	if contents == nil {
		logAndQuit(fmt.Sprint("Invalid url", url))
	}

	playlist, listType, _ := m3u8.Decode(*bytes.NewBuffer(contents), false)

	if listType == m3u8.MASTER {
		masterPl := playlist.(*m3u8.MasterPlaylist)
		numstreams = uint(len(masterPl.Variants))
	}

	return listType, numstreams
}

/*
 Prints an example usage string
*/
func printUsage() {
	fmt.Println()
	fmt.Println("Example usage")
	fmt.Println("========================================")
	fmt.Println("voot-dl.exe -url=http://video.voot.com/fhls/04145_a/video.m3u8 -bitrate=102400 -seqstart=10 -outfile=video.mp4")
	fmt.Println("This download the hls video at the specified url and bitrate starting from sequence 10")

	fmt.Println()
	fmt.Println("voot-dl.exe -url=http://video.voot.com/fhls/04145_a/video.m3u8 -stream=1 -outfile=video.ts")
	fmt.Println("This download the hls video at the specified url and stream number\n")
	fmt.Println()
}

func main() {
	var the_url string
	var outfilepath string

	flag.StringVar(&the_url, "url", "", "Full url to m3u8 file (including http/https)")
	flag.StringVar(&outfilepath, "outfile", "", "Path to output file. If it exists it will be opened in append mode")
	flag.UintVar(&bitrate, "bitrate", 0, "Bitrate of stream to download. If not specified, list available bitrates")
	flag.Uint64Var(&startSeq, "seqstart", 0, "Sequence number of part to start downloading. Useful for resuming downloads. If not specified will download from begining")
	flag.UintVar(&streamno, "stream", 0, "Alternative to specifying the bitrate. For variant master playlists having multiple bitrates this indicates the stream no of the part to download. The first stream has a number of 1.")
	flag.StringVar(&proxyUrl, "proxy", "", "Proxy url to use in the form of http://10.10.10.10:8080")
	flag.Parse()

	if len(the_url) == 0 {
		fmt.Println("\nError: Parameter -url not specified")
		printUsage()
		flag.PrintDefaults()
		return
	}

	var err error
	if _, err = url.ParseRequestURI(the_url); err != nil {
		fmt.Println("\nError: Invalid url")
		return
	}

	if len(proxyUrl) > 0 {
		if _, err = url.ParseRequestURI(proxyUrl); err != nil {
			fmt.Println("\nError: Invalid proxy url")
			return
		}
		os.Setenv("HTTP_PROXY", proxyUrl)
		logOnly("Using proxy " + proxyUrl)
	}

	// Get the playlist
	listType, numstreams := getPlaylist(the_url)

	// Display playlist contents only if neither of bitrate, streamno is specified and the playlist type is a master playlist having multiple bitrates
	if bitrate == 0 && streamno == 0 && listType == m3u8.MASTER && numstreams > 1 {
		fmt.Println("The specified stream is a variant master playlist having multiple bitrates. Need to specify one of -bitrate or -stream. Listing available bitrates")
		listBitrates(the_url)
		return
	}

	if streamno > numstreams {
		fmt.Println("\nError: Specified -stream number is invalid. Maximum possible value is ", numstreams-1)
		return
	}

	if len(outfilepath) == 0 {
		fmt.Println("\nError: Parameter -outfile not specified")
		printUsage()
		flag.PrintDefaults()
		return
	}

	if _, err = os.Stat(outfilepath); err == nil {
		logOnly("Output file exists. Opening in append mode")
		outfile, err = os.OpenFile(outfilepath, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
		if err != nil {
			log.Fatal(err)
		}
	} else {
		outfile, err = os.Create(outfilepath)
		if err != nil {
			log.Fatal(err)
		}
	}
	defer outfile.Close()

	if listType == m3u8.MASTER {
		parseMasterPlaylist(the_url)
	} else {
		parseMediaPlaylist(the_url)
	}
}
