package main

import (
	"fmt"
	"os"
	"time"
)

func logAndQuit(msg string) {
	t := time.Now()
	fmt.Println(t.Format("[3:04:05 PM]"), msg)
	os.Exit(1)
}

func logOnly(msg string) {
	t := time.Now()
	fmt.Println(t.Format("[3:04:05 PM]"), msg)
}

func logSameLine(msg string) {
	t := time.Now()
	fmt.Print(t.Format("[3:04:05 PM]"), msg)
}
