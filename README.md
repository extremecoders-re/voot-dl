### voot-dl

A tool to download videos from voot.com

### Usage

```
C:\>voot-dl.exe -url=http://cdnapi.voot.com/.../.../index.m3u8 -bitrate=333420 -outfile=video.ts
```
Note that the outfile is opened in append mode.

To list the available bitrates run

```
C:\>voot-dl.exe -url=http://cdnapi.voot.com/.../.../index.m3u8
```
It will list the available bitrates.

You can also specify a proxy to use

```
voot-dl.exe -url=http://cdnapi.voot.com/.../.../index.m3u8 -bitrate=333420 -proxy=http://10.0.25.185:8080 -outfile=video.ts 
```